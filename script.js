const userScoreText = document.getElementById("user-scoreboard")
const botCounter = document.getElementById("bot-counter");
let botPoints = 0;
let userPoints = 0;
let currentRound = 1;


// Modal
const btnRounds = document.getElementById("button-rounds");

btnRounds.addEventListener("click", getModalInfo);
function getModalInfo(){
    const rounds = document.getElementById("rounds").value;
    let nickname = document.getElementById("nickname").value;

    if (nickname == "") {
        nickname = "Player"
    }

    closeModal()
    return [parseInt(rounds), nickname]
}

function closeModal () {
    let modal = document.getElementById("init")
    modal.style.display = "none"
}

// Game
const userButton = document.getElementsByClassName("button");
for(let i = 0; i<userButton.length; i++){
    userButton[i].addEventListener("click", game);
}

function game() {
    let btnId = this.id
    let roundWinner = playResult(playUser(btnId), playBot());
    let modalInfo = getModalInfo()
    let gameRounds = modalInfo[0]
    let nick = modalInfo[1]

    if (roundWinner == 0) {
        userPoints++;
    }
    userScoreText.innerHTML = `${nick}: ${userPoints}`

    if (roundWinner == 1) {
        botPoints++;
    }
    botCounter.innerHTML = botPoints;

    if ((currentRound == gameRounds) && (userPoints > botPoints)) {
        finalWinner(`congratulations ${nick}, you win 😜🥇`, "lime")
    }
    else if((currentRound == gameRounds) && (botPoints > userPoints)) {
        finalWinner("game over! bot win 🦾🤖", "red")
    }
    else if((currentRound == gameRounds) && (botPoints == userPoints)) {
        finalWinner("the game ended in a draw 😬🤨", "orange")
    }

    roundInterval()
    changeRound(gameRounds)
}

function changeRound(gameRounds) {
    const roundCounter = document.getElementById("current-round")
    roundCounter.textContent = `${currentRound}/${gameRounds}`
    currentRound++;
}

function playResult(userPlay, botPlay) {
    const result = document.getElementById("result");
    let winner = -1; // 0 = user ; 1 = bot

    result.innerText = "";

    if (userPlay == botPlay) {
        result.style.color = "orange";
        result.textContent = "Draw! 😬";
    } 
    else if ((userPlay === 0) && (botPlay === 2)) {
        result.style.color = "lime";
        result.textContent = "You Win! 😎";
        winner = 0;
    }
    else if ((userPlay === 1) && (botPlay === 0)) {
        result.style.color = "lime";
        result.textContent = "You Win! 😎";
        winner = 0;
    }
    else if ((userPlay === 2) && (botPlay === 1)) {
        result.style.color = "lime";
        result.textContent = "You Win! 😎";
        winner = 0;
    }
    else {
        result.style.color = "red";
        result.textContent = "You Lose! 🤖";
        winner = 1;
    }
    return winner;
}

function playUser (id) {
    const userPlayDiv = document.getElementById("user-play");
    let userPlay = -1;
    let userPlayText = document.createElement("span");

    if (id == "user-rock") {
        userPlay = 0;
        userPlayText.textContent = "you chose ROCK";
    }
    if (id == "user-paper") {
        userPlay = 1;
        userPlayText.textContent = "you chose PAPER";
    }
    if (id == "user-scissors") {
        userPlay = 2;
        userPlayText.textContent = "you chose SCISSORS";
    }

    userPlayDiv.innerHTML = "";
    userPlayDiv.appendChild(userPlayText);

    return userPlay;
}

function playBot() {
    const botPlayDiv = document.getElementById("bot-play");
    let botPlayText = document.createElement("span");
    let botPlay = Math.floor(Math.random() * (3-1+1));

    if (botPlay == 0) {
        botPlayText.textContent = "bot chose ROCK";
        changeBotButtonBorder(0);
    }
    if (botPlay == 1) {
        botPlayText.textContent = "bot chose PAPER";
        changeBotButtonBorder(1);
    }
    if (botPlay == 2) {
        botPlayText.textContent = "bot chose SCISSORS";
        changeBotButtonBorder(2);
    }

    botPlayDiv.innerHTML = "";
    botPlayDiv.appendChild(botPlayText);

    return botPlay;
}

function changeBotButtonBorder (btnId) {
    const botButton = document.getElementsByClassName("bot-button");
    const pinkColor = "rgba(216, 81, 205, 0.5)";
    const blueColor = "rgba(75, 120, 255, 0)";

    if (btnId == 0) {
        botButton[0].style.borderColor = pinkColor;
        botButton[1].style.borderColor = blueColor;
        botButton[2].style.borderColor = blueColor;
    }
    if (btnId == 1) {
        botButton[1].style.borderColor = pinkColor;
        botButton[2].style.borderColor = blueColor;
        botButton[0].style.borderColor = blueColor;
    }
    if (btnId == 2) {
        botButton[2].style.borderColor = pinkColor;
        botButton[0].style.borderColor = blueColor;
        botButton[1].style.borderColor = blueColor;
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

async function roundInterval() {
    userButton[0].setAttribute("disabled", "disabled")
    userButton[1].setAttribute("disabled", "disabled")
    userButton[2].setAttribute("disabled", "disabled")
    await sleep(1000)
    userButton[0].removeAttribute("disabled")
    userButton[1].removeAttribute("disabled")
    userButton[2].removeAttribute("disabled")
}

function finalWinner(gameWinner, color) {
    const finalWinnerDiv = document.getElementById("final-winner")
    let winnerElement = document.createElement("span")
    winnerElement.className = "winner"

    winnerElement.textContent = gameWinner
    winnerElement.style.color = color

    finalWinnerDiv.style.display = "flex"
    finalWinnerDiv.appendChild(winnerElement)
}